#include<iostream>//bibliotecas del sistema se importan usando "<"
#include"myRoundFunc.h"//cuando creamos nuestras propias librerias, se importa con comillas y el linker une todo al compilar
#include"myRoundFunc.h"
using namespace std;

int myRound(double);//esto es como "prometer" al codigo que algun rato definiremos el metodo "factorial"
int main()
{
	cout << "Ingrese un numero: ";
	double n;
	bool p = (cin >> n).good();//este metodo good() verifica si el cin esta correcto
	if(p){
		int r = myRound(n);
		cout << "Resultado de redondeo: " << r << "\n";
	}else cout << "Ocurrio un error al procesar la entrada\n";
}



