#include<iostream>//bibliotecas del sistema se importan usando "<"
#include"isPairFunc.h"//cuando creamos nuestras propias librerias, se importa con comillas y el linker une todo al compilar
#include"isPairFunc.h"
using namespace std;

bool isPair(int);//esto es como "prometer" al codigo que algun rato definiremos el metodo "factorial"
int main()
{
	cout << "Ingrese un numero: ";
	int n;
	bool p = (cin >> n).good();//este metodo good() verifica si el cin esta correcto
	if(p){
		bool r = isPair(n);
		if(r) cout << "El numero " << n << " es par\n";
		else cout << "El  numero " << n << " no es par\n";
	}else cout << "Ocurrio un error al procesar la entrada\n";
}


