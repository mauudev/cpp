#include <stdlib.h>
#include <iostream>
using namespace std;

int fc(const void* a, const void* b){
	int* p = (int*)a;
	int* q = (int*)b;

	return *p - *q;
}

int main(){
	int s [] = {6,8,5,4,3,6,5,0};
	qsort(s,8,sizeof(int),fc);
	for(int e : s)
		cout << e << endl;
}