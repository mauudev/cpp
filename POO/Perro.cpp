#include<iostream>
#include<cstring>
using namespace std;
class Animal{
public:
	size_t size;
	size_t edad;
};

class Perro:public Animal{//de esta forma decimos que todo lo que es publico en Animal, sera publico en perro y todos los demas accesors
public:
	char raza[16];
};

int main(){
	Animal s{60,5};
	cout << s.size << endl;

	Perro p;
	strcpy(p.raza, "chapi");
	cout << p.raza << endl;
	p.size = 40;
	cout << p.size << endl;
}