#include<stdlib.h>
#include<stdio.h>
#include<string.h>

void sum(int a, int b){
	int c = a + b;
	printf("%d\n",c);
}

void sub(int a, int b){
	int c = a - b;
	printf("%d\n",c);
}

void r(){
	printf("HI");
}
int main(){
	void (*p)(int,int); // creamos el puntero a una funcion que retorna void y recibe dos int
	p = sum;
	p(6,10);
	p = sub;
	p(3,8);
	p = r;
}