#include<iostream>
#include<cstring>
using namespace std;

class Student{
	size_t id;
	char* name; //mejor para no desperdiciar memoria, solo apunta donde esta la cadena

	public:
	Student(size_t id, const char* name){
		this->id = id;
		size_t len = strlen(name);
		this->name = new char[len+1];
		strcpy(this->name,name);
	}	
	void show(){
		cout << name << " " << id << "\n";
	}
};
int main(){
	Student s1{123,"Josepe"};
	s1.show();
}
