#include <iostream>
#include <cstring>
using namespace std;

void disemvowel(const char* s){
	size_t len = strlen(s);
	auto nn = new char[len+1];
	memcpy(nn,s,len+1);
	char dict[] = {'a','e','i','o','u',0};
	size_t dict_len = strlen(dict);
	cout << dict_len << endl;
	size_t i,j = 0;
	char res[200];
	while(i < len){
		for(size_t k = 0; k <= dict_len; k++){
			if(nn[i] == dict[k]){
				//cout << "Es una vocal" << endl;
				res[j] = '*';
				j++;
				break;
			}else{ 
				//cout << "No es una vocal" << endl;
				res[j] = nn[i];
				//cout << res << endl;
				j++;
				break;
			}
		}
		i++;
	}
	res[i] = 0;
	cout << res << endl;
}

int main(){
	disemvowel("This is a test");
}