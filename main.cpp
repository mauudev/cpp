#include<iostream>//bibliotecas del sistema se importan usando "<"
#include"myFuncs.h"//cuando creamos nuestras propias librerias, se importa con comillas y el linker une todo al compilar
#include"myFuncs.h"
using namespace std;

int factorial(int);//esto es como "prometer" al codigo que algun rato definiremos el metodo "factorial"
int main()
{
	cout << "Ingrese un numero: ";
	int n;
	bool p = (cin >> n).good();//este metodo good() verifica si el cin esta correcto
	if(p){
		int r = factorial(n);
		cout << "Factorial " << r << "\n";
	}else cout << "Ocurrio un error al procesar la entrada\n";
}



