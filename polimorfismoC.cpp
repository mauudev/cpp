#include<cstdlib>
#include<iostream>
#include<cstring>
using namespace std;

typedef struct{
	int age;
	int type;
	void (*hablar)();
	void (*comer)();
}Animal;

typedef struct {
	Animal a;
	int n_eggs;
}Oviparo;

void perro_hablar(){puts("GAU");}
void perro_comer(){puts("PERRO COMIENDO");}
void gato_hablar(){puts("MEOW");}
void gato_comer(){puts("GATO COMIENDO");}
void pollo_hablar(){puts("KIKIRIKI");}
void pollo_comer(){puts("POLLO COMIENDO");}

void new_perro(Animal* a, int age){
	a->age = age;
	a->hablar = perro_hablar;
	a->comer = perro_comer;
}

void new_gato(Animal* g, int age){
	g->type = 2;
	g->age = age;
	g->hablar = gato_hablar;
	g->comer = gato_comer;
}

void new_pollo(Oviparo* o, int age, int n_eggs){
	o->a.type = 3;
	o->a.age = age;
	o->a.hablar = pollo_hablar;
	o->a.comer = pollo_comer;
	o->n_eggs = n_eggs;
}

void say_something(Animal* a){
	a->hablar();
	if(a->type == 3){
		Oviparo* o = (Oviparo*)a;
		printf("%d\n",o->n_eggs);
	}	
}

int main(){
	Animal perro;
	Animal gato;
	new_perro(&perro,15);
	new_gato(&gato,1);

	perro.comer();
	perro.hablar();
	gato.comer();
	gato.hablar();

	Oviparo pollo;
	new_pollo(&pollo, 1,200);
	say_something((Animal*)&pollo);
}