#include<iostream>
#include<cstring>
using namespace std;

class Persona{
	private:
	char nombre[32];
	char apellido[32];
	size_t edad;

	public:
	/*void init(const char* nombre, const char* apellido, size_t edad){
		strcpy((*this).nombre,nombre);
		strcpy((*this).apellido,apellido);
		//strcpy(this->nombre,nombre);
		this->edad = edad;
	}*/
	Persona(const char* nombre, const char* apellido, size_t edad){
		strcpy((*this).nombre,nombre);
		strcpy((*this).apellido,apellido);
		//strcpy(this->nombre,nombre);
		this->edad = edad;
	}
	void show(){
		cout << nombre << " " << apellido << " " << edad << "\n";
	}
};

int main(){
	Persona p{"Evo","Perez",77};
	//p.init("Evo","Perez",60);

	p.show();
	//cout << p.nombre << "\n";
}