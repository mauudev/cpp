#include <iostream>
#include <cstring>
using namespace std;

void int_sort(int* s){
	size_t len = sizeof(s);
	len--;
	//cout << len << endl;
	bool permutar = true;
	size_t i = 0;
	while(permutar){
		permutar = false;
		i++;
		for(size_t j = 0; j < (len - i); j++){
			//cout << j << endl;
			if(s[j] > s[j+1]){
				permutar = true;
				int temp = s[j];
				//cout << s[j]<<";" << s[j+1] << endl;
				s[j] = s[j+1];
				s[j+1] = temp;
			}
		}
	}
	for(size_t k = 0; k < len; k++) cout << s[k] << endl;
}

void PrintSize(int *p_someArray) {
    printf("%zu\n", sizeof(p_someArray));
}

int main(){
	int s[] = {4,3,6,3,3,7,1};
	int_sort(s);
}