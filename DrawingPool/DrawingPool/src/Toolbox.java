import java.util.ArrayList;
public class Toolbox {
	private ArrayList<Tool> flowDiagramCategory;
	private ArrayList<Tool> UMLCategory;
	private ArrayList<Tool> entityRelationCategory;
	
	public Toolbox() {
		flowDiagramCategory = new ArrayList<Tool>();
		UMLCategory = new ArrayList<Tool>();
		entityRelationCategory = new ArrayList<Tool>();
	}
	public void addFlowDiagramTool(Tool t) {
		flowDiagramCategory.add(t);
	}
	public void addUMLCategoryTool(Tool t) {
		UMLCategory.add(t);
	}
	public void addEntityRelationCategory(Tool t) {
		entityRelationCategory.add(t);
	}
	public void removeFlowDiagramTool(Tool t) {
		flowDiagramCategory.remove(t);
	}
}
