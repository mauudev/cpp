import java.util.ArrayList;
public class CanvasContainer {
	private ArrayList<Tool> toolContainer;
	private String name;
	private int XDimension;
	private int YDimension;
	private int totalDimension;
	
	public CanvasContainer(String name, int XDimension, int YDimension) {
		this.name = name;
		this.XDimension = XDimension;
		this.YDimension = YDimension;
		totalDimension = XDimension * YDimension;
		toolContainer = new ArrayList<Tool>();
	}
	public String getName() {return name;}
	public int getTotalDimension() {return totalDimension;}
	public ArrayList<Tool> getToolContainer(){return toolContainer;}
	public void addTool(Tool t) {
		toolContainer.add(t);
		System.out.println("New Tool added to canvas !");
	}
	public void removeTool(Tool t) {
		toolContainer.remove(t);
		System.out.println("Tool removed from canvas !");
	}
	public void showToolContainer() {
		for(Tool t: toolContainer)
			t.drawTool();
	}
}
