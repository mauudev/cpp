import java.awt.Canvas;

public class Event {
	
	public void dragToolToCanvas(Tool t, CanvasContainer c) {
		c.addTool(t);
	}
	public void removeToolFromCanvas(Tool t, CanvasContainer c) {
		c.removeTool(t);
	}
	public void connectTools(Tool t1, Tool t2, ConnectionLine c) {
		c.setInitialConnection(t1);
		c.setEndConnection(t2);
		System.out.println("Connection done from: "+t1.getName()+" to: "+t2.getName());
	}
	public void removeConnection(ConnectionLine c) {
		c.removeConnection();
	}
}
