public class Terminator extends FlowDiagramTool {
	private boolean isStart,isEnd;
	
	public Terminator(String name, String shape,String label, String description, int XCoordenate, int YCoordenate,boolean isStart, boolean isEnd) {
		super(name,shape,label,description,XCoordenate,YCoordenate);
		this.isStart = isStart;
		this.isEnd = isEnd;
	}
	public boolean isStart() {return isStart;}
	public boolean isEnd() {return isEnd;}
}
