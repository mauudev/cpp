import java.util.ArrayList;
public class Process extends FlowDiagramTool{
	private ArrayList<CanvasContainer> subProcess;
	
	public Process(String name, String shape,String label, String description, int XCoordenate, int YCoordenate) {
		super(name,shape,label,description,XCoordenate,YCoordenate);
		subProcess = new ArrayList<CanvasContainer>();
	}
	public ArrayList<CanvasContainer> getSubProcess(){return subProcess;}
	public void addSubProcess(CanvasContainer c) {subProcess.add(c);}
	public void showSubProcess() {
		for(CanvasContainer c: subProcess) {
			c.showToolContainer();
		}
	}
}
