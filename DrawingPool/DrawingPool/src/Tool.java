public class Tool {
	private String name;
	private String shape;
	private int XCoordenate;
	private int YCoordenate;
	private String label;
	private String description;
	
	public Tool(String name, String shape,String label, String description, int XCoordenate, int YCoordenate) {
		this.name = name;
		this.shape = shape;
		this.XCoordenate = XCoordenate;
		this.YCoordenate = YCoordenate;
		this.label = label;
		this.description = description;
	}
	public String getName() {return name;}
	public String getShape() {return shape;}
	public String getLabel() {return label;}
	public String getDescription() {return description;}
	public int getXCoordenate() {return XCoordenate;}
	public int getYCoordenate() {return YCoordenate;}
	
	public void setName(String name) {this.name = name;}
	public void setShape(String shape) {this.shape = shape;}
	public void setXCoordenate(int XC) {XCoordenate = XC;}
	public void setYCoordenate(int YC) {YCoordenate = YC;}
	public void drawTool() {
		System.out.println("Tool "+getShape()+" '"+getName()+" "+getLabel()+" "+getDescription()+"' at X: "+ XCoordenate+" and Y: "+YCoordenate+"\n");
	}
}
